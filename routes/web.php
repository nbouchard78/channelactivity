<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', ['as' => 'homeRoute', 'uses' => 'ChatController@index'])->middleware('auth');
//Route::get('/channel/{channelName}', ['as' => 'channelRoute', 'uses' => 'ChatController@channelInfo'])->middleware('auth');
Route::post('/channel', ['as' => 'channelProcess', 'uses' => 'ChatController@processChannel'])->middleware('auth');
Route::get('/reset', ['as' => 'resetCacheRoute', 'uses' => 'ChatController@reset'])->middleware('auth');

Route::get('/comment/{channelId}/{characterId}', ['as' => 'commentRoute', 'uses' => 'ChatController@editComments'])->middleware('auth');
Route::post('/comment', ['as' => 'saveCommentRoute', 'uses' => 'ChatController@saveComment'])->middleware('auth');

Route::get('login/debug', [ 'as' => 'debugLogin', 'uses' => 'Auth\LoginController@debugLogin']);
Route::get('logout', [ 'as' => 'logoutRoute', 'uses' => 'Auth\LoginController@logout'])->middleware('auth');

Route::get('login', [ 'as' => 'login', 'uses' => 'Auth\LoginController@login']);
Route::get('login/eveonline', [ 'as' => 'loginEveonline', 'uses' => 'Auth\LoginController@redirectToProvider']);
Route::get('login/eveonline/callback', ['as' => 'loginRedirect', 'uses' => 'Auth\LoginController@handleProviderCallback']);