# Channel activity

## Project references

### Orthrus
Michael Mach's library: https://github.com/michaelcooke/orthrus

### laravel/socialite
Laravel Socialite library: https://laravel.com/docs/5.5/socialite

### Eve SSO Socialite provider
Lib: https://github.com/jrtashjian/socialiteprovider-eveonline
