<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Chat channel cleanup</title>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        
    </head>
    <body>
        <div class="container-fluid">
        <h1>Channels</h1>
        <hr>
    
        <ul class="list-inline">
            <li class="list-inline-item"></li>
            <div class="container">
            <h2>Access list</h2>

                {{-- <div class="container">
                    Owner: @include('characterCompact', ['model' => $channel->owner])
                </div> --}}

                @if (isset($channel->warning))
                <h3>Warnings ({{ count($channel->warning) }})</h3>
                <div class="container">
                @foreach($channel->warning as $allowed)
                    @include('characterCompact', ['model' => $allowed])
                @endforeach
            </div>
            @endif

                @if( isset($channel->allowed))
                    <h3>Allowed ({{ count($channel->allowed) }})</h3>
                    <div class="container">
                    @foreach($channel->allowed as $allowed)
                        @if($allowed->accessor_type === "character")
                            @include('characterCompact', ['model' => $allowed->character])
                        @elseif($allowed->accessor_type === "corporation")
                            Corporation: {{ $allowed->corporation->name }}
                            @if(isset($allowed->corporation["alliance"]))
                                ({{ $allowed->corporation->alliance->name }})
                            @endif
                        @endif
                    @endforeach
                </div>
                @endif


                {{-- @if( isset($channel->operators))
                <h3>Operators ({{ count($channel->operators) }})</h3>
                <div class="container">
                @foreach($channel->operators as $allowed)
                    @if($allowed->accessor_type === "character")
                        @include('characterCompact', ['model' => $allowed->character])
                    @elseif($allowed->accessor_type === "corporation")
                        Corporation: {{ $allowed->corporation->name }}
                        @if(isset($allowed->corporation["alliance"]))
                            ({{ $allowed->corporation->alliance->name }})
                        @endif
                    @endif
                @endforeach
            </div>
            @endif --}}
            </div>
            </div>

            <div class="container">
                @foreach($channel->alliances as $name => $allianceCount)
                <div class="row">
                    <div class="col">{{ $name }}</div><div class="col">{{ $allianceCount }}</div>
                </div>
                @endforeach
            </div>
    
            <hr>

            <div class="container">
                <textarea class="form-control" rows="30">{{ $channel->warningText }}</textarea>
            </div>

        </li>            
        </ul>
    </div>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
    </body>
</html>
