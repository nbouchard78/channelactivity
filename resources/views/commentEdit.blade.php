<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Chat channel cleanup</title>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        
    </head>
    <body>

        <h1>Comment edition</h1>

        <form method="post" action="{{ route('saveCommentRoute') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="charId" value="{{ $model->charId }}">
            <input type="hidden" name="channelId" value="{{ $model->channelId }}">
            
            <div class="form-group row">
                <label for"staticName" class="col-sm-1 col-form-label">Character</label>
                <div class="col-sm-8">
                    <input type="text" readonly class="form-control-plaintext" id="staticName" value="{{ $model->name }}">
                </div>
            </div>

            <div class="form-group row">
                <label for="staticCorp" class="col-sm-1 col-form-label">Corporation</label>
                <div class="col-sm-8">
                    <input type="text" readonly class="form-control-plaintext" id="staticCorp" value="{{ $model->corporation }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="note" class="col-sm-1 col-form-label">Note:</label>
                <div class="col-sm-8">
                    <textarea class="form-control" name="note" rows="3">{{ $model->note }}</textarea>
                </div>
            </div>

            <a class="btn btn-primary" href="{{ route('channelRoute', ['channelName' => 'Profidence']) }}">Back</a>
            <button class="btn btn-primary" type="submit">Save</button>
        </form>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
    </body>
</html>
