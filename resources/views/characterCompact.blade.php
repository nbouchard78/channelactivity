<!-- /resources/view/characterCompact.blade.php -->

<div class="row">
    <div style="background:{{ $model->standingColor }};color:#FFFFFF;">&nbsp;&nbsp;&nbsp;</div>
    <div class="col-sm-3"><a href="{{ $model->zkillUrl }}" target="killboard">[{{ $model->corporation->ticker }}] {{ $model->name }}</a></div>
    <div class="col-sm-3"><a href="{{ $model->corporation->zkillUrl }}" target="killboard">{{ $model->corporation->name }}</a></div>
    <div class="col-sm-3">
    @if(isset($model->corporation->alliance))
        <a href="{{ $model->corporation->alliance->zkillUrl }}" target="killboard">{{ $model->corporation->alliance->name }}</a>
    @else
    &nbsp;
        @endif
    </div>
    <div closs="col-sm-3">
    @if(isset($model->lastKill))
        @if(isset($model->lastKill->url))
            <a href="{{ $model->lastKill->url }}" target="killmail">{{ $model->lastKill->date->toDateString() }}</a>
        @else
            {{ $model->lastKill->date->toDateString() }}
        @endif
    @else
        No killboard activity
    @endif
    </div>
</div>
<div class="row">
    <div class="col-sm-8">
        {{ $model->note }} 
    </div>
    @if(Permissions::canEditNote())
    <div class="col-sm-3">
        <a href="{{ route('commentRoute', ['channelId' => '-57605977', 'characterId' => $model->character_id])}}">Edit note</a>
    </div>
    @endif
</div>
<hr>
