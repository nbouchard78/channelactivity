<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharacterCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('character_comments', function (Blueprint $table) {
            $table->increments('id');

            $table->string('channelId');
            $table->string('characterId');
            $table->text('note')->nullable();

            $table->timestamps();

            $table->index('characterId');
            $table->index(['channelId', 'characterId']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('character_comments');
    }
}
