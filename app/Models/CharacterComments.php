<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cache;

class CharacterComments extends Model
{
    protected $fillable = ['channelId', 'characterId', 'note'];


    public static function FindCharacterNote(string $channel, string $characterId)
    {
        $cache = $channel . '.' . $characterId;
        $note = null;

        if(Cache::has($cache))
            $note = Cache::get($cache);

        if (is_null($note)) {
            $comment = CharacterComments::where('channelId', $channel)
                ->where('characterId', $characterId)
                ->first();

            if (!is_null($comment)) {
                $note = $comment->note;
            }
            Cache::put($cache, $note, 60*24);
        }

        return $note;
    }

    /**
     * Save (update or create) note
     *
     * @param string $channel Channel ID
     * @param string $charId  Character ID
     * @param string $note    Note
     * 
     * @return void
     */
    public static function saveComment(string $channel, string $charId, $note)
    {
        $comment = CharacterComments::updateOrCreate(
            ['channelId' => $channel, 'characterId' => $charId],
            ['note' => $note]
        );

        $cache = $channel . '.' . $charId;
        Cache::forget($cache);
        Cache::forget('chatChannels');
    }
}
