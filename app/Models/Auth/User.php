<?php
/**
 * 
 */
namespace App\Models\Auth;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

/**
 * Undocumented class
 */
class User extends Model implements AuthenticatableContract
{
    use Authenticatable;

    protected $fillable = ['name', 'characterId'];
    protected $hidden = ['remember_token'];
}
