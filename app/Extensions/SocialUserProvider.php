<?php
/**
 * 
 */
namespace App\Extensions;
 
use Illuminate\Support\Str;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Auth\Authenticatable;

/**
 * Undocumented class
 */
class SocialUserProvider implements UserProvider
{
    /**
     * Retrieve a user by the given credentials.
     *
     * @param array $credentials Credention to retrieve
     * 
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        try
        {
            $user = \App\Models\Auth\User::where('characterId', $credentials['characterId'])->firstOrFail();
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $ex)
        //catch(ModelNotFoundException $ex)
        {
            $user = \App\Models\Auth\User::create(
                ['characterId' => $credentials['characterId'], 'name' => $credentials['characterName']]
            );
            //$user->characterID = $credentials['characterID'];
            //$user->name = $credentials['characterName'];
            $user->save();
        }
        return $user;        
    }
  
    /**
     * Validate a user against the given credentials.
     *
     * @param \Illuminate\Contracts\Auth\Authenticatable $user        t
     * @param array                                      $credentials Request credentials
     * 
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, Array $credentials)
    {
        return true;
    }

    /**
     * Undocumented function
     *
     * @param [type] $identifier description
     * 
     * @return void
     */
    public function retrieveById($identifier) 
    {
        $user = null;

        $qry = \App\Models\Auth\User::where('id', '=', $identifier);
        if ($qry->count() > 0) {
            $user = $qry->first();
        }
        return $user;        
    }

    /**
     * Undocumented function
     *
     * @param [type] $identifier description
     * @param [type] $token      description
     * 
     * @return void
     */
    public function retrieveByToken($identifier, $token) 
    {

    }

    /**
     * Undocumented function
     *
     * @param Authenticatable $user  test
     * @param [type]          $token test
     * 
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token) 
    {

    }
}