<?php
/**
 * 
 */
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Contracts\Auth\Guard;
use Request;
use Log;

use Socialite;
use Auth;
use Session;
use App;

/**
 * Undocumented class
 */
class LoginController extends Controller
{
    /**
     * Undocumented function
     *
     * @return void
     */
    public function debugLogin()
    {
        Auth::attempt(['characterName' => 'tntimmy', 'characterId' => '667730805']);
        return redirect('/');
    }

    public function login()
    {
        return view('login');
    }

    /**
     * Undocumented function
     *
     * @param [type] $social t
     * 
     * @return void
     */
    public function redirectToProvider()
    {
        return Socialite::driver('eveonline')->redirect();
    }

    /**
     * Undocumented function
     *
     * @param [type] $social t
     * 
     * @return void
     */
    public function handleProviderCallback()
    {
        $userSocial = Socialite::driver('eveonline')->user();
        Auth::attempt(['characterName' => $userSocial->name, 'characterId' => $userSocial->id]);

        return redirect('/');
    }

    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect('/');
    }
}
