<?php
/**
 * Helper function class
 * 
 * PHP Version 7
 * 
 * @category Helper_Class
 * @package  ChannelActivity
 * @author   Nicolas Bouchard <nicolas.bouchard@something.com>
 * @license  http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link     http://pear.php.net/package/PackageName
 */

namespace App\Http\Controllers;

ini_set('max_execution_time', 1800);

use Illuminate\Http\Request;
use ESI;
use Carbon\Carbon;
use Log;
use Illuminate\Support\Facades\Cache;

use Auth;
use Socialite;

use \App\Helpers;

/**
 * Helper class ot help query ESI
 * 
 * @category Helper_Class
 * @package  ChannelActivity
 * @author   Nicolas Bouchard <nicolas.bouchard@something.com>
 * @license  http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version  Release: @package_version@
 * @link     http://pear.php.net/package/PackageName
 */
class ChatController extends Controller
{
    private $_helper;

    /**
     * Constructor
     */
    function __construct()
    {
    }

    /**
     * Process index
     * 
     * @return view view to display
     */
    public function index()     
    {
        $user = Auth::user();
        $model = (object)null;
        $model->people = "";

        return view('home')->with('user', $user)->with('model', $model);
    }

    public function processChannel(Request $request)
    {
        $user = Auth::user();

        $model = (object)null;
        $model->people = explode("\r\n", $request->input('people'));

        $this->_helper = new \App\Helpers\ChannelAccessHelper();
        $model->result = $this->_helper->processUsersList($model->people);

        return view('channellist')->with('channel', $model->result);
        //return view('home')->with('user', $user)->with('model', $model);
    }

    public function editComments($channelId, $characterId)
    {
        $this->_helper = new \App\Helpers\ChannelAccessHelper();

        $charInfo = $this->_helper->getCharacterInfo($characterId, $channelId);

        $model = (object)null;
        $model->charId = $characterId;
        $model->channelId = $channelId;
        $model->name = $charInfo->name;
        $model->corporation = $charInfo->corporation->name;
        $model->note = $charInfo->note;

        return view('commentEdit')->with('model', $model);
    }

    public function saveComment(Request $request)
    {
        //$all = $request->all();

        $note = $request->input('note');
        $charId = $request->input('charId');
        $channelId = $request->input('channelId');

        \App\Models\CharacterComments::saveComment($channelId, $charId, $note);

        return redirect()->route('channelRoute', ['channelName' => 'Profidence']);
    }

    public function reset()
    {
        Log::info('Cache reseted.');
        Cache::flush();
 
        return $this->index();
    }
}
