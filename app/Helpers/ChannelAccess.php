<?php

/**
 * Helper function class
 * 
 * PHP Version 7
 * 
 * @category Helper_Class
 * @package  ChannelActivity
 * @author   Nicolas Bouchard <nicolas.bouchard@something.com>
 * @license  http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link     http://pear.php.net/package/PackageName
 */

namespace App\Helpers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Log;
//use ESI;
use Seat\Eseye;
use Illuminate\Support\Facades\Cache;

/**
 * Helper class ot help query ESI
 * 
 * @category Helper_Class
 * @package  ChannelActivity
 * @author   Nicolas Bouchard <nicolas.bouchard@something.com>
 * @license  http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version  Release: @package_version@
 * @link     http://pear.php.net/package/PackageName
 */
class ChannelAccessHelper
{
    private $_context;  // used by calls to zkillboard  

    private $_defaultCache = 60 * 24;

    private $_allianceStanding = null;

    private $_esi = null;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $options = array(
            'http' => array(
                'user_agent' => 'custom user agent string',
                'verify_peer' => false,
                'verify_peer_name' => false,
                'timeout' => 5
            ));
        $this->_context = stream_context_create($options);

        $authentication = new \Seat\Eseye\Containers\EsiAuthentication([
            'client_id' => env('ESEYE_CLIENT_ID'),
            'secret' => env('ESEYE_SECRET_KEY'),
            'refresh_token' => env('ESEYE_REFRESH_TOKEN'),
        ]);

        $this->_esi = new \Seat\Eseye\Eseye($authentication);

        $this->_allianceStanding = $this->_getAllianceContacts();
    }

    //region Channel list processing
    public function processUsersList($userList)
    {
        $channel = (object)null;
        $channel->allowed = [];
        $channel->channel_id = -57605977;

        // Get Character ID
        foreach($userList as $user) {
            $test = $this->_esi
                ->setQueryString([
                    'categories' => 'character',
                    'search' => $user,
                    'strict' => 'true'
                ])
                ->invoke('get', '/search/');

            $allowed = (object)null;
            $allowed->accessor_type = "character";
            $allowed->accessor_id = $test->character[0];            

            array_push($channel->allowed, $allowed);
            //$channel->allowed->push($allowed);
        }

        return $this->processChannelList($channel);
    }
    
    /** 
     * Query and process channel list
     * 
     * @return class Channel list
     */
    public function processChannelList($channel)
    {
        /*$channelsList = $this->_getCache('chatChannels');
        if (!is_null($channelsList)) {
            $this->_flagIssues($channelsList);

            //foreach ($channelsList as $channel) {
                //if ($channel->channel_id != -57605977) continue;                
                arsort($channelsList->alliances);
            //}
            return $channelsList;
        }*/

        //$channelsList = ESI::character(667730805)->getChatChannels();

        //foreach ($channelsList as $channel) {
        //$channel = $channelsList;
            //if ($channel->channel_id != -57605977) continue;

            //$channel->owner = $this->getCharacterInfo($channel->owner_id, $channel->channel_id);

            $channel->alliances = [];

            foreach ($channel->allowed as $allowed) {
                if ($allowed->accessor_type === "character") {
                    $allowed->character 
                        = $this->getCharacterInfo($allowed->accessor_id, $channel->channel_id);

                    if (isset($allowed->character->corporation["alliance"])) {
                        if (!array_key_exists($allowed->character->corporation->alliance->name, $channel->alliances)) {
                            $channel->alliances[$allowed->character->corporation->alliance->name] = 0;
                        }
                        $channel->alliances[$allowed->character->corporation->alliance->name]++;
                    } else {
                        $unaffiliated = "Unaffiliated";

                        if (!array_key_exists($unaffiliated, $channel->alliances)) {
                            $channel->alliances[$unaffiliated] = 0;
                        }
                        $channel->alliances[$unaffiliated]++;
                    }
                } else if ($allowed->accessor_type === "corporation") {
                    $allowed->corporation = $this->_getCorporationInfo($allowed->accessor_id);
                } else if ($allowed->accessor_type === "alliance") {
                    $allowed->alliance = $this->_getAllianceInfo($allowed->accessor_id);
                }
            }

            $this->_orderAccessList($channel->allowed);

            /*foreach ($channel->operators as $allowed) {
                if ($allowed->accessor_type === "character") {
                    $allowed->character = $this->getCharacterInfo($allowed->accessor_id, $channel->channel_id);
                } else if ($allowed->accessor_type === "corporation") {
                    $allowed->corporation = $this->_getCorporationInfo($allowed->accessor_id);
                } else if ($allowed->accessor_type === "alliance") {
                    $allowed->alliance = $this->_getAllianceInfo($allowed->accessor_id);
                }
            }

            $this->_orderAccessList($channel->operators);*/

            arsort($channel->alliances);
            
        //}

        //Cache::put('chatChannels', $channel, 5);

        $this->_flagIssues($channel);
        
        return $channel;
    }

    /**
     * Order the access list via the killboard date
     * 
     * @param list $accessList 
     * 
     * @return list
     */
    private function _orderAccessList(&$accessList)
    {
        usort(
            $accessList, function ($a, $b) {
                $aValue = null;
                $bValue = null;

                if (isset($a->character)) {
                    if (isset($a->character->lastKill)) {
                        $aValue = $a->character->lastKill->date;
                    }
                }
                if (isset($b->character)) {
                    if (isset($b->character->lastKill)) {
                        $bValue = $b->character->lastKill->date;
                    }
                }
                return $aValue <=> $bValue;
            }
        );
    }

    /**
     * Undocumented function
     *
     * @param array $channelsList channels list
     * 
     * @return void
     */
    private function _flagIssues(&$channel)
    {
        $channel->warning = [];
        $warningText = [];

        foreach ($channel->allowed as $allowed) {
            $charInfo = $allowed->character;

            if ($charInfo->standing < 5) {
                array_push($channel->warning, $charInfo);
                array_push($warningText, '[' . $charInfo->corporation->ticker . '] ' . $charInfo->name );
            }
        }

        $channel->warningText = implode("\r\n", $warningText);

    }

    //endregion

    //region Cache related functions
    /**
     * Get information from the cache
     * 
     * @param string $key Key to retrieve
     * 
     * @return class Information from the cache or null
     */
    private function _getCache($key)
    {
        try{
            return Cache::get($key);
        }
        catch (\Exception $e) { }
        
        return null;
    }

    /**
     * Save information into the cache
     * 
     * @param string $key      Key to save
     * @param object $value    Information to save
     * @param int    $duration Length to save the information for.
     * 
     * @return nothing
     */
    private function _setCache($key, $value, $duration = null)
    {
        if (is_null($duration)) {
            $duration = $this->_defaultCache;
        }

        if ($duration != -1) {
            Cache::put($key, $value, $duration);
        } else {
            Cache::forever($key, $value);
        }

    }
    //endregion
    
    //region zKillboard queries

    /**
     * Query zkillboard to get kill information
     * 
     * @param string $scope Which scope to query (character, corporation, alliance)
     * @param int    $id    Entity ID to query
     * 
     * @return class killmail if there is one
     */
    private function _getLastKillboardActivity($scope, $id)
    {
        $return = null;

        /*try {
            $url = "https://zkillboard.com/api/" . $scope . "/" . $id .
                "/limit/1/orderDirection/desc/";

            $result = file_get_contents($url, false, $this->_context);

            if ($result) {
                $zkillData = json_decode($result);

                if (count($zkillData) > 0) {
                    $return = $zkillData[0];
                }
            }
        }
        catch (\Exception $e) {
            Log::error('Error while querying zkillboard: '. $e->getMessage());
        }*/

        return $return;
    }

    /**
     * Query zkillboard to get kill information
     * 
     * @param string $scope Which scope to query (character, corporation, alliance)
     * @param int    $id    Entity ID to query
     * 
     * @return class killmail if there is one
     */
    public function getLastKillboard($scope, $id)
    {
        $cacheKey = "kb" . $scope . $id;

        $lastKill = $this->_getCache($cacheKey);

        if (!is_null($lastKill)) {
            return $lastKill;
        }

        $lastKill = $this->_getLastKillboardActivity($scope, $id);
        if (isset($lastKill)) {
            $lastKill->date = Carbon::parse($lastKill->killmail_time);
            $lastKill->url = "https://zkillboard.com/kill/" . 
                $lastKill->killmail_id . "/";
            $this->_setCache($cacheKey, $lastKill);
        }

        return $lastKill;
    }
    //endregion

    //region ESI queries per entity
    /**
     * Retrieve character information from ESI
     * 
     * @param int $charId Character ID to query
     * 
     * @return class Character information
     */
    public function getCharacterInfo($charId, $channelId = '')
    {
        $cacheKey = 'InfoCharacter' . $charId;

        $charInfo = $this->_getCache($cacheKey);
        if (is_null($charInfo)) {
            $charInfo = $this->_esi->invoke('get', '/characters/{character_id}/', ['character_id' => $charId]);
            $charInfo->character_id = $charId;
            $charInfo->corporation 
                = $this->_getCorporationInfo($charInfo->corporation_id);

            $charInfo->zkillUrl = "https://zkillboard.com/character/". $charId ."/";

            $this->_setCache($cacheKey, $charInfo);
        }

        $charInfo->lastKill = $this->getLastKillboard("character", $charId);
        $charInfo->standing = $this->_getRelativeStanding("character", $charId, $charInfo);
        $charInfo->standingColor = $this->_convertStandingToColor($charInfo->standing);

        // Retrieve character note from the database
        $charInfo->note = \App\Models\CharacterComments::FindCharacterNote($channelId, $charId);
        
        return $charInfo;
    }

    /**
     * Retrieve corporation information from ESI
     * 
     * @param int $corp_id Corporation ID to query
     * 
     * @return class Corporation information
     */
    private function _getCorporationInfo($corp_id)
    {
        $cacheKey = 'InfoCorp' . $corp_id;

        $corpInfo = $this->_getCache($cacheKey);


        if (is_null($corpInfo)) {
            $corpInfo = $this->_esi->invoke('get', '/corporations/{corporation_id}/', ['corporation_id' => $corp_id]);
            $corpInfo->corporation_id = $corp_id;
            $corpInfo->zkillUrl = "https://zkillboard.com/corporation/". $corp_id ."/";

            if (isset($corpInfo["alliance_id"])) { 
                $corpInfo->alliance = $this->_getAllianceInfo($corpInfo->alliance_id); 
            } 
            $this->_setCache($cacheKey, $corpInfo);
        }

        $corpInfo->standing = $this->_getRelativeStanding("corporation", $corp_id, $corpInfo);

        return $corpInfo;
    }

    /**
     * Retrieve the alliance information from EIS
     * 
     * @param int $alliance_id Alliance ID to query
     * 
     * @return class Alliance information
     */
    private function _getAllianceInfo($alliance_id)
    {
        $cacheKey = "InfoAlliance" . $alliance_id;
        $alliance = $this->_getCache($cacheKey);        
        if (is_null($alliance)) {
            $alliance = $this->_esi->invoke('get', '/alliances/{alliance_id}/', ['alliance_id' => $alliance_id]);
            $alliance->alliance_id = $alliance_id;
            $alliance->zkillUrl = "https://zkillboard.com/alliance/". $alliance_id ."/";

            $this->_setCache($cacheKey, $alliance);
        }

        $alliance->standing = $this->_getRelativeStanding("alliance", $alliance_id, $alliance);

        return $alliance;
    }
    //endregion

    //region Retrieve alliance contacts
    /**
     * Retrieve the alliance contacts
     * 
     * @return nothing
     */
    private function _getAllianceContacts()
    {
        $cacheKey = 'InfoAllianceStanding';
        $allianceId = 1988009451; // CVA

        $allianceStanding = null;
        $allianceStanding = $this->_getCache($cacheKey);
        if (is_null($allianceStanding)) {            
            Log::debug('Alliance standing read from ESI.');

            $page = 0;
            $standing = [];

            do {
                $page = $page + 1;

                $allianceStanding = $this->_esi
                    ->setQueryString([
                        'page' => $page])                
                    ->invoke('get', '/alliances/{alliance_id}/contacts/', ['alliance_id' => $allianceId]);

                foreach (json_decode($allianceStanding->raw) as $contact) {
                    $key = $contact->contact_type . ":" . $contact->contact_id;
                    $standing[$key] = $contact->standing;
                }

            } while($page <= $allianceStanding->pages);

            $this->_setCache($cacheKey, $standing, 60);
            $allianceStanding = $standing;
        }
       
        return $allianceStanding;
    }

    /**
     * Convert standing to color
     * 
     * @param int $standing standing to convert
     * 
     * @return color
     */
    private function _convertStandingToColor($standing) 
    {
        if ($standing == 10) {
            return "#0000FF";
        } else if ($standing == 5) {
            return "#66ACF3";
        } else if($standing == 0) {
            return "#999999";
        } else if($standing == -5) {
            return "#FF5733";
        } else {
            return "#FF0000";
        }
    }

    /**
     * Retrieve relative standing
     * 
     * @param string $type   test
     * @param int    $id     test
     * @param class  $entity test
     * 
     * @return int standing
     */
    private function _getRelativeStanding($type, $id, $entity)
    {
        $key = $type . ":" . $id;
        if (array_key_exists($key, $this->_allianceStanding)) {
            return $this->_allianceStanding[$key];
        } 

        if ($type === "character") {
            return $this->_getRelativeStanding("corporation", $entity->corporation->corporation_id, $entity->corporation);
        } else if ($type === "corporation" && isset($entity->alliance)) {
            return $this->_getRelativeStanding("alliance", $entity->alliance->alliance_id, $entity->alliance);
        } else if ($type === "alliance" && $id === 1988009451) {
            return 10;
        } else {
            return 0;
        }
    }
    //endregion
}