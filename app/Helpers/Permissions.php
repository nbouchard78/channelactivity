<?php

/**
 * Helper function class
 * 
 * PHP Version 7
 * 
 * @category Helper_Class
 * @package  ChannelActivity
 * @author   Nicolas Bouchard <nicolas.bouchard@something.com>
 * @license  http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link     http://pear.php.net/package/PackageName
 */

namespace App\Helpers;

use Auth;
use Illuminate\Support\Facades\Cache;

/**
 * Helper class ot help query ESI
 * 
 * @category Helper_Class
 * @package  ChannelActivity
 * @author   Nicolas Bouchard <nicolas.bouchard@something.com>
 * @license  http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version  Release: @package_version@
 * @link     http://pear.php.net/package/PackageName
 */
class Permissions
{
    /**
     * Undocumented function
     *
     * @return boolean
     */
    public static function canEditNote()
    {
        try 
        {
            /*$user = Auth::user();
            $cache = 'EditAccess' . $user->characterId;

            $canEdit = Cache::get($cache);

            if (!is_null($canEdit)) {
                return $canEdit;
            }

            $helper = new ChannelAccessHelper();
            $channelsList = $helper->processChannelList();

            $canEdit = false;

            foreach ($channelsList as $channel) {
                if ($channel->channel_id == -57605977) {
                    foreach ($channel->operators as $oper) {
                        if ($oper->character->character_id == $user->characterId) {
                            $canEdit = true;
                            break;
                        }
                    }
                }
            }

            Cache::put($cache, $canEdit, 10);*/

            //return $canEdit;
            return false;
        }
        catch (\Exception $e)
        {
            return false;
        }
    }
}