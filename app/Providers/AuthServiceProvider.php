<?php

namespace App\Providers;

use Auth;
use App\Extensions;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $this->app->bind(
            'App\Models\Auth\User', 
            function ($app) {
                return new \App\Models\Auth\User();
            }
        );

        // add custom guard provider
        Auth::provider(
            'social', 
            function ($app, array $config) {
                return new \App\Extensions\SocialUserProvider($app->make('\App\Models\Auth\User'));
            }
        );

        // add custom guard
        /*Auth::extend(
            'customToken',
            function ($app, $name, array $config) {
                return new \App\Services\Auth\CustomTokenGuard(
                    Auth::createUserProvider($config['provider']), 
                    $app->make('request')
                );
            }
        );*/
    }

    public function register()
    {
        // Nothing here
    }
}
