<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers;

class UpdateCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:updatecache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update data cache based on access lists';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $helper = new \App\Helpers\ChannelAccessHelper();
        $helper->processChannelList();
    }
}
